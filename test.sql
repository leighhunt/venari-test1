-- SELECT @@VERSION

-- sp_helpdb

-- USE venari-test1

-- SELECT DB_NAME()

-- DROP TABLE sweet_login

CREATE TABLE sweet_login
(
    username VARCHAR(MAX),
    theme VARCHAR(255),
    version VARCHAR(255),
    event_timestamp DATETIME2 DEFAULT current_timestamp
)


INSERT INTO sweet_login
(username, theme, version)
VALUES('leighghunt', 'Harvest', '0.0')

SELECT * FROM sweet_login
ORDER BY event_timestamp DESC

-- DELETE FROM sweet_login

-- SELECT CURRENT_timestamp