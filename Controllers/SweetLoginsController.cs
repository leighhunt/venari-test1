using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace venari_test1.Controllers
{
    public class SweetLoginEvent{
        public string Username {get; set;}
        public string Theme {get; set;}
        public string Version {get; set;}
        public DateTime EventTimestamp {get; set;}
    }

    [Route("api/[controller]")]
    [ApiController]
    public class SweetLoginsController : ControllerBase
    {
        public SweetLoginsController(IConfiguration configuration, ILogger<SweetLoginsController> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }
        public ILogger _logger { get; }

                private string GetConnectionString(){
            string dbUsername = Configuration["DBUsername"];
            string dbPassword = Configuration["DBPassword"];
            string connectionString = Configuration.GetConnectionString("DefaultConnection").Replace("{username}", dbUsername).Replace("{password}", dbPassword);

            return connectionString;
        }

        private SqlConnection GetConnection(){
            try{
                SqlConnection sqlConnection = new SqlConnection(GetConnectionString());

                sqlConnection.Open();

                return sqlConnection;
            }
            catch(Exception ex){
                _logger.LogError(ex, "Unable to connect to database.");
                throw;
            }
        }

        public IEnumerable<Dictionary<string, object>> Serialize(SqlDataReader reader)
        {
            var results = new List<Dictionary<string, object>>();
            var cols = new List<string>();
            for (var i = 0; i < reader.FieldCount; i++) 
                cols.Add(reader.GetName(i));

            while (reader.Read()) 
                results.Add(SerializeRow(cols, reader));

            return results;
        }
        private Dictionary<string, object> SerializeRow(IEnumerable<string> cols, 
                                                        SqlDataReader reader) {
            var result = new Dictionary<string, object>();
            foreach (var col in cols) 
                result.Add(col, reader[col]);
            return result;
        }
        
        // GET api/values
        [HttpGet]
        public ActionResult<string> Get()
        {
            try{
                _logger.LogInformation("Hello from SweetLogins Get ()");
                // Didn't work:
                // System.Diagnostics.Debug.WriteLine("DEBUG WriteLine Hello from SweetLogins Get ()");
                // System.Diagnostics.Trace.WriteLine("TRACE WriteLine Hello from SweetLogins Get ()");

                SqlConnection sqlConnection = GetConnection();
                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM sweet_login ORDER BY event_timestamp DESC", sqlConnection);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                
                var r = Serialize(reader);
                string json = JsonConvert.SerializeObject(r, Formatting.Indented);

                return json;
            }
            catch(Exception ex){
                System.Diagnostics.Trace.WriteLine(ex);
                _logger.LogError(ex, "Couldn't get logins");
                throw;
            }

            // return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            _logger.LogInformation("Hello from SweetLogins Get (id)");
            string appSecret = Configuration["AppSecret"];
            _logger.LogInformation($"appSecret: {appSecret}");
            return $"AppSecret: {appSecret}, connectionString: {GetConnectionString()}";
            
            // return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromForm] SweetLoginEvent sweetLoginEvent)
        {
            // _logger.LogInformation(sweetLoginEvent.Username);
            // _logger.LogInformation(sweetLoginEvent.Theme);
            // _logger.LogInformation(sweetLoginEvent.Version);
            // _logger.LogInformation(sweetLoginEvent.EventTimestamp.ToString());
            sweetLoginEvent.EventTimestamp = DateTime.Now;

            SqlConnection sqlConnection = GetConnection();
            string sql = $"INSERT INTO sweet_login VALUES('{sweetLoginEvent.Username}', '{sweetLoginEvent.Theme}', '{sweetLoginEvent.Version}', '{sweetLoginEvent.EventTimestamp.ToString("o")}')";

            _logger.LogInformation(sql);

            SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
            sqlCommand.ExecuteNonQuery();                
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
