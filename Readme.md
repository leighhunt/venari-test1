GET

```
GET /api/SweetLogins HTTP/1.1
Host: localhost:5001
User-Agent: PostmanRuntime/7.18.0
Accept: */*
Cache-Control: no-cache
Postman-Token: 20819292-97b4-4dd4-b6c5-50ab520ce502,7828f756-793c-4172-b150-6aed57bcc9b0
Host: localhost:5001
Accept-Encoding: gzip, deflate
Connection: keep-alive
cache-control: no-cache
```

Example result:
```
/ 20191016083825
// https://venari-test1.azurewebsites.net/api/SweetLogins

[
  {
    "username": "leighghunt-post test",
    "theme": "Harvest",
    "version": "0.0",
    "event_timestamp": "2019-10-15T16:24:18.768177"
  },
  {
    "username": "leighghunt-post test 2",
    "theme": "Harvest",
    "version": "0.1",
    "event_timestamp": "2019-10-15T16:25:30.837065"
  },
  {
    "username": "leighghunt-post test 3",
    "theme": "Harvest",
    "version": "0.1",
    "event_timestamp": "2019-10-16T08:17:50.669516"
  }
]
```

POST

```
POST /api/SweetLogins HTTP/1.1
Host: localhost:5001
Content-Type: application/json
User-Agent: PostmanRuntime/7.18.0
Accept: */*
Cache-Control: no-cache
Postman-Token: 71cf545a-9ea0-4207-9443-dd1bfba78b21,4af707be-39e4-48d5-9318-ea6a67d40355
Host: localhost:5001
Accept-Encoding: gzip, deflate
Content-Length: 90
Connection: keep-alive
cache-control: no-cache

{
    "username": "leighghunt-post test 3",
    "theme": "Harvest",
    "version": "0.1"
}
```

# Notes:



https://azure.microsoft.com/en-us/resources/samples/key-vault-dotnet-core-quickstart/

dotnet user-secrets set "AppSecret" "MySecret_DEV"
dotnet user-secrets set "DBUsername" "leigh"
dotnet user-secrets set "DBPassword" "<password>"

DEV test - https://localhost:5001/api/values/1 = MySecret_DEV


https://docs.microsoft.com/en-us/azure/key-vault/managed-identity
https://docs.microsoft.com/en-us/aspnet/core/security/key-vault-configuration?view=aspnetcore-3.0


App Service -> Identity - enable system assigned identity
Or via CLI:
    $ az webapp identity assign --name venari-test1 --resource-group HNRG-Testing
    {
        "principalId": "621b7067-da6a-479c-9e3b-cb08c4112962",
        "tenantId": "a988a73b-6a78-4005-bb7c-25bf0acdf4d5",
        "type": "SystemAssigned",
        "userAssignedIdentities": null
    }


    $ az keyvault set-policy --name venari-test1-keyvault --object-id 621b7067-da6a-479c-9e3b-cb08c4112962 --secret-permissions get list 


az keyvault create --name "venari-test1-keyvault" --resource-group "HNRG-Testing" --location "australiaeast"

az keyvault secret set --vault-name "venari-test1-keyvault" --name "AppSecret" --value "MySecret_Azure"
az keyvault secret set --vault-name "venari-test1-keyvault" --name "DBUsername" --value "leigh"
az keyvault secret set --vault-name "venari-test1-keyvault" --name "DBPassword" --value "<password>"

az keyvault secret set --vault-name "venari-test1-keyvault" --name "SecretName" --value "secret_value_1_prod"
az keyvault secret set --vault-name "venari-test1-keyvault" --name "Section--SecretName" --value "secret_value_2_prod"